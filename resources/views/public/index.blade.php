@extends('bpanel4-public.layouts.regular-page')
@section('title')
    Mi lista de deseos
@endsection
@livewireStyles
@section('content')
    <div class="dv-container">
        @include('bpanel4-clients::public.my-account.menu')
        <h1>Mi lista de deseos</h1>
        <div class="regular-page-container my-orders-page">
            <div class="row">
                <div class="col-12 products-wishlist">
                    @livewire('bpanel4-product-wishlist::product-list')
                </div>
            </div>
        </div>
    </div>
@endsection
@livewireScripts
