<div class="add-to-wishlist">
    @auth
        @if($productIsInWishlist)
            <button wire:click="removeProductFromWishlist"><i class="fas fa-heart"></i></button>
        @else
            <button wire:click="addProductToWishlist"><i class="far fa-heart"></i></button>
        @endif
    @endauth

    @pushonce('scripts')
        <script>
          document.addEventListener('DOMContentLoaded', function () {
            Livewire.on('added-to-wishlist', event => {
              toastr.success('Producto añadido a la lista de deseos');
            });

            Livewire.on('removed-from-wishlist', event => {
              toastr.success('Producto quitado de la lista de deseos');
            });
          });
        </script>
    @endpushonce
</div>