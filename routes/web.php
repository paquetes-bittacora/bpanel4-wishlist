<?php

declare(strict_types=1);

use Bittacora\Bpanel4\ProductWishlist\Http\Controllers\WishlistPublicController;
use Illuminate\Support\Facades\Route;

Route::prefix('mi-cuenta/wishlist')->name('bpanel4-product-wishlist.')->middleware(['web'])
    ->group(static function (): void {
        Route::get('/mostrar', [WishlistPublicController::class, 'index'])->name('show');
    });
