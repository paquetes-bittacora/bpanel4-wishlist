<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductWishlist\Actions;

use Bittacora\Bpanel4\Clients\Contracts\Client;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\ProductWishlist\Models\WishlistProduct;

final class AddProductToWishlist
{
    public function execute(Product $product, Client $client): void
    {
        $wishlistProduct = new WishlistProduct();
        $wishlistProduct->product()->associate($product);
        $wishlistProduct->client()->associate($client);
        $wishlistProduct->save();
    }
}
