<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductWishlist\Actions;

use Bittacora\Bpanel4\Clients\Contracts\Client;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\ProductWishlist\Models\WishlistProduct;

final class RemoveProductFromWishlist
{
    public function execute(Product $product, Client $client): void
    {
        (new WishlistProduct())->where('product_id', $product->getId())
            ->where('client_id', $client->getClientId())->delete();
    }
}
