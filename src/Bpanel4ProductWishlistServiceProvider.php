<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductWishlist;

use Bittacora\Bpanel4\HooksComponent\Services\Hooks;
use Bittacora\Bpanel4\ProductWishlist\Hooks\AdditionalMyAccounMenuItemsHook;
use Bittacora\Bpanel4\ProductWishlist\Http\Livewire\AddToWishlistButton;
use Bittacora\Bpanel4\ProductWishlist\Http\Livewire\WishlistProductList;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\ServiceProvider;
use Livewire\LivewireManager;

final class Bpanel4ProductWishlistServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-product-wishlist';

    /**
     * @throws BindingResolutionException
     */
    public function boot(LivewireManager $livewire, Hooks $hooks): void
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->registerLivewireComponents($livewire);
        $this->registerHooks($hooks);
    }

    public function registerLivewireComponents(LivewireManager $livewire): void
    {
        $livewire->component(
            self::PACKAGE_PREFIX . '::add-to-wishlist-button',
            AddToWishlistButton::class
        );

        $livewire->component(
            self::PACKAGE_PREFIX . '::product-list',
            WishlistProductList::class
        );
    }

    public function registerHooks(Hooks $hooks): void
    {
        $hooks->register(
            'additional-my-account-menu-items',
            [$this->app->make(AdditionalMyAccounMenuItemsHook::class), 'renderButton'](...)
        );
    }
}
