<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductWishlist\Hooks;

use Illuminate\View\Factory;

final class AdditionalMyAccounMenuItemsHook
{
    public function __construct(private readonly Factory $view)
    {
    }

    public function renderButton(): void
    {
        echo $this->view->make('bpanel4-product-wishlist::public.hooks.additional-my-account-menu-items-hook')
            ->render();
    }
}
