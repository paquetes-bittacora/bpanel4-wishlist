<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductWishlist\Http\Controllers;

use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Clients\Services\ClientService;
use Illuminate\Contracts\View\View;
use Illuminate\View\Factory;

final class WishlistPublicController
{
    public function __construct(
        private readonly Factory $view,
        private readonly ClientService $clientService,
    ) {
    }

    /**
     * @throws UserNotLoggedInException
     */
    public function index(): View
    {
        return $this->view->make('bpanel4-product-wishlist::public.index', [
            'client' => $this->clientService->getCurrentClient(),
        ]);
    }
}
