<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductWishlist\Http\Livewire;

use Bittacora\Bpanel4\Clients\Contracts\Client;
use Bittacora\Bpanel4\Clients\Contracts\ClientService;
use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\ProductWishlist\Actions\AddProductToWishlist;
use Bittacora\Bpanel4\ProductWishlist\Actions\RemoveProductFromWishlist;
use Bittacora\Bpanel4\ProductWishlist\Models\WishlistProduct;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;
use Webmozart\Assert\Assert;

final class AddToWishlistButton extends Component
{
    private ?ClientService $clientService = null;
    private ?Factory $view = null;
    public int $productId = 0;
    public bool $productIsInWishlist = false;

    public function boot(Factory $view, ClientService $clientService): void
    {
        $this->view = $view;
        $this->clientService = $clientService;
    }

    public function render(): View
    {
        Assert::isInstanceOf($this->view, Factory::class);
        $this->productIsInWishlist = $this->productIsInWishlist();
        return $this->view->make('bpanel4-product-wishlist::public.livewire.add-to-wishlist-button');
    }

    public function addProductToWishlist(AddProductToWishlist $addProductToWishlist): void
    {
        $product = $this->getProduct();
        $client = $this->getClient();
        $addProductToWishlist->execute($product, $client);
        $this->emit('added-to-wishlist');
    }

    public function removeProductFromWishlist(RemoveProductFromWishlist $removeProductFromWishlist): void
    {
        $product = $this->getProduct();
        $client = $this->getClient();
        $removeProductFromWishlist->execute($product, $client);
        $this->emit('removed-from-wishlist');
    }

    public function getProduct(): Product
    {
        return Product::whereId($this->productId)->firstOrFail();
    }

    public function getClient(): Client
    {
        Assert::isInstanceOf($this->clientService, ClientService::class);
        return $this->clientService->getCurrentClient();
    }

    public function productIsInWishlist(): bool
    {
        Assert::isInstanceOf($this->clientService, ClientService::class);
        try {
            return (new WishlistProduct())->where('product_id', $this->productId)
                ->where('client_id', $this->clientService->getCurrentClient()->getClientId())->exists();
        } catch (UserNotLoggedInException) {
            return false;
        }
    }
}
