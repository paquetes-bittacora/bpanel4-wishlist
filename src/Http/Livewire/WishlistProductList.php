<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductWishlist\Http\Livewire;

use Bittacora\Bpanel4\Clients\Exceptions\UserNotLoggedInException;
use Bittacora\Bpanel4\Clients\Services\ClientService;
use Bittacora\Bpanel4\Products\Http\Livewire\ProductsList;
use Bittacora\Bpanel4\Products\Models\Product;
use Bittacora\Bpanel4\ProductWishlist\Models\WishlistProduct;
use Illuminate\Database\Eloquent\Builder;

class WishlistProductList extends ProductsList
{
    public function buildQuery(): Builder
    {
        $query = Product::where('active', '=', 1)->whereIn('id', $this->getProductsInWishlist());
        $query->orderBy('name->' . $this->app->getLocale(), 'ASC');
        return $query;
    }

    /**
     * @return string[]|int[]
     * @throws UserNotLoggedInException
     */
    protected function getProductsInWishlist(): array
    {
        $clientService = $this->app->make(ClientService::class);
        /** @phpstan-ignore-next-line  */
        return WishlistProduct::whereClientId($clientService->getCurrentClient()->getClientId())
            ->pluck('product_id')->toArray();
    }
}
