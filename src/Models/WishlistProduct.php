<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductWishlist\Models;

use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Products\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Bittacora\Bpanel4\ProductWishlist\Models\WishlistProduct
 *
 * @property int $id
 * @property int $product_id
 * @property int $client_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Client $client
 * @property-read Product $product
 * @method static Builder|WishlistProduct newModelQuery()
 * @method static Builder|WishlistProduct newQuery()
 * @method static Builder|WishlistProduct query()
 * @method static Builder|WishlistProduct whereClientId($value)
 * @method static Builder|WishlistProduct whereCreatedAt($value)
 * @method static Builder|WishlistProduct whereId($value)
 * @method static Builder|WishlistProduct whereProductId($value)
 * @method static Builder|WishlistProduct whereUpdatedAt($value)
 * @method Builder|WishlistProduct where(string $string, int $productId)
 * @mixin \Eloquent
 */
final class WishlistProduct extends Model
{
    /**
     * @return BelongsTo<Product, WishlistProduct>
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * @return BelongsTo<Client, WishlistProduct>
     */
    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }
}
