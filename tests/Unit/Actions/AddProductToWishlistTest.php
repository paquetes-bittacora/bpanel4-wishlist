<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductWishlist\Tests\Unit\Actions;

use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\ProductWishlist\Actions\AddProductToWishlist;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class AddProductToWishlistTest extends TestCase
{
    use RefreshDatabase;
    private AddProductToWishlist $addProductToWishlist;

    protected function setUp(): void
    {
        parent::setUp();
        $this->addProductToWishlist = $this->app->make(AddProductToWishlist::class);
    }

    public function testAnadeElProductoALaWishlistDelClienteConectado(): void
    {
        $client = (new ClientFactory())->createOne();
        $product = (new ProductFactory())->createOne();

        $this->addProductToWishlist->execute($product, $client);

        $this->assertDatabaseHas('wishlist_products', [
            'product_id' => $product->getId(),
            'client_id' => $client->getClientId(),
        ]);
    }
}
