<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\ProductWishlist\Tests\Unit\Actions;

use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Products\Database\Factories\ProductFactory;
use Bittacora\Bpanel4\ProductWishlist\Actions\AddProductToWishlist;
use Bittacora\Bpanel4\ProductWishlist\Actions\RemoveProductFromWishlist;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class RemoveProductFromWishlistTest extends TestCase
{
    use RefreshDatabase;

    private AddProductToWishlist $addProductToWishlist;
    private RemoveProductFromWishlist $removeProductFromWishlist;

    protected function setUp(): void
    {
        parent::setUp();
        $this->removeProductFromWishlist = $this->app->make(RemoveProductFromWishlist::class);
        $this->addProductToWishlist = $this->app->make(AddProductToWishlist::class);
    }

    public function testAnadeElProductoALaWishlistDelClienteConectado(): void
    {
        $client = (new ClientFactory())->createOne();
        $product = (new ProductFactory())->createOne();
        $this->addProductToWishlist->execute($product, $client);
        $this->assertDatabaseHas('wishlist_products', [
            'product_id' => $product->getId(),
            'client_id' => $client->getClientId(),
        ]);

        $this->removeProductFromWishlist->execute($product, $client);

        $this->assertDatabaseMissing('wishlist_products', [
            'product_id' => $product->getId(),
            'client_id' => $client->getClientId(),
        ]);
    }
}
